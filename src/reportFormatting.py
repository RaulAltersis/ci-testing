import sys

def jmeterFormatting():
    # ./public/
    #f = open("C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/plot.html", "r+")
    f = open("./public/plot.html", "r+")
    html = f.readlines()
    f.close()

    bokeh = ""

    for line in html:
        bokeh = bokeh + line
        if "cdn.bokeh" in line:
            bokehJS = line
    
    bokehBody = (bokeh[bokeh.find('<body>')+6:bokeh.find('</body>')])

    customGraphs = '<li><a href="plot.html"><i class="fa fa-dashboard fa-fw"></i> Custom Graphs</a></li>'
    customGraphsLink = '<li><a href="../../plot.html"><i class="fa fa-dashboard fa-fw"></i> Custom Graphs</a></li>'
    htmlEnd = '</div></div></div><script src="sbadmin2-1.0.7/bower_components/jquery/dist/jquery.min.js"></script><script src="sbadmin2-1.0.7/bower_components/bootstrap/dist/js/bootstrap.min.js"></script><script src="sbadmin2-1.0.7/bower_components/flot/excanvas.min.js"></script><script src="sbadmin2-1.0.7/bower_components/flot/jquery.flot.js"></script><script src="sbadmin2-1.0.7/bower_components/flot/jquery.flot.pie.js"></script><script src="sbadmin2-1.0.7/bower_components/flot/jquery.flot.resize.js"></script><script src="sbadmin2-1.0.7/bower_components/flot/jquery.flot.time.js"></script><script src="sbadmin2-1.0.7/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script><script src="sbadmin2-1.0.7/bower_components/flot-axislabels/jquery.flot.axislabels.js"></script><script src="sbadmin2-1.0.7/bower_components/metisMenu/dist/metisMenu.min.js"></script><script src="content/js/dashboard-commons.js"></script><script src="content/js/dashboard.js"></script><script src="sbadmin2-1.0.7/dist/js/sb-admin-2.js"></script><script type="text/javascript" src="content/js/jquery.tablesorter.min.js"></script></body></html>'

    #f = open("C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/index.html")
    f = open("./public/index.html")
    index = f.readlines()
    f.close()

    # Formatting plot page
    #f = open("C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/plot.html", "w")
    f = open("./public/plot.html", "w")
    for line in index:
        if '</head>' in line:
            line = bokehJS + line
        elif '<ul class="nav" id="side-menu">' in line:
            line = line + customGraphs
        elif '<div class="row">' in line:
            line = line + bokehBody + htmlEnd
            f.write(line)
            break            
        f.write(line)
    f.close()


    # Including link in menu in all pages
    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/index.html', "w+")
    f = open('./public/index.html', "w+")
    for line in index:
        if '<ul class="nav" id="side-menu">' in line:
            line = line + customGraphs
        f.write(line)
    f.close()

    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/content/pages/CustomsGraphs.html', "r")
    f = open('./public/content/pages/CustomsGraphs.html', "r")
    index = f.readlines()
    f.close()
    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/content/pages/CustomsGraphs.html', "w+")
    f = open('./public/content/pages/CustomsGraphs.html', "w+")
    for line in index:
        if '<ul class="nav" id="side-menu">' in line:
            line = line + customGraphsLink
        f.write(line)
    f.close()

    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/content/pages/OverTime.html', "r")
    f = open('./public/content/pages/OverTime.html', "r")
    index = f.readlines()
    f.close()
    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/content/pages/OverTime.html', "w+")
    f = open('./public/content/pages/OverTime.html', "w+")
    for line in index:
        if '<ul class="nav" id="side-menu">' in line:
            line = line + customGraphsLink
        f.write(line)
    f.close()

    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/content/pages/ResponseTimes.html', "r")
    f = open('./public/content/pages/ResponseTimes.html', "r")
    index = f.readlines()
    f.close()
    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/content/pages/ResponseTimes.html', "w+")
    f = open('./public/content/pages/ResponseTimes.html', "w+")
    for line in index:
        if '<ul class="nav" id="side-menu">' in line:
            line = line + customGraphsLink
        f.write(line)
    f.close()

    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/content/pages/Throughput.html', "r")
    f = open('./public/content/pages/Throughput.html', "r")
    index = f.readlines()
    f.close()
    #f = open('C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/content/pages/Throughput.html', "w+")
    f = open('./public/content/pages/Throughput.html', "w+")
    for line in index:
        if '<ul class="nav" id="side-menu">' in line:
            line = line + customGraphsLink
        f.write(line)
    f.close()




def main(argv):
    if "jmeter" in argv:
        jmeterFormatting()

if __name__ == "__main__":
   main(sys.argv[1])