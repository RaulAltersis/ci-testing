from bokeh.io import output_file, show
from bokeh.layouts import column, row
from bokeh.plotting import figure
import requests
import json
import sys
from datetime import datetime

def main(argv):
    
    x = []
    y = []

    # Reading CPU metrics from AppD
    response = requests.get(argv[3] + "/controller/rest/applications/Server & Infrastructure Monitoring/metric-data?metric-path=Application Infrastructure Performance|Root|Individual Nodes|CHBSLJME05TST|Hardware Resources|CPU|%Busy&time-range-type=BEFORE_NOW&duration-in-mins=90&rollup=false&output=JSON", auth=(argv[0] + '@' + argv[1], argv[2]))

    # Ingesting response
    cpu = json.loads(response.content)

    # Getting graph values
    for value in cpu[0]['metricValues']:
        x.append(datetime.fromtimestamp(value['startTimeInMillis']/1000))
        y.append(value['value'])

    # Setting graph
    p = figure(plot_width=960, plot_height=300, x_axis_type="datetime", y_axis_label="CPU Busy %", title="CPU Busy (%)", title_location="above")
    p.title.text_font_size = '14pt'
    p.line(x, y, color='navy', alpha=1, line_width=2)

    # Reading memory metrics from AppD
    response = requests.get(argv[3] + "/controller/rest/applications/Server & Infrastructure Monitoring/metric-data?metric-path=Application Infrastructure Performance|Root|Individual Nodes|CHBSLJME05TST|Hardware Resources|Memory|Used %&time-range-type=BEFORE_NOW&duration-in-mins=90&rollup=false&output=JSON", auth=(argv[0] + '@' + argv[1], argv[2]))

    # Ingesting response
    mem = json.loads(response.content)

    x = []
    y = []

    # Getting graph values
    for value in mem[0]['metricValues']:
        x.append(datetime.fromtimestamp(value['startTimeInMillis']/1000))
        y.append(value['value'])

    # Setting graph
    q = figure(plot_width=960, plot_height=300, x_axis_type="datetime", y_axis_label="Mem Used %", title="Mem Used (%)", title_location="above")
    q.title.text_font_size = '14pt'
    q.line(x, y, color='red', alpha=1, line_width=2)

    # Setting output
    u = column(p, q)

    # Output location for local tests -- comment for pipeline use
    #output_file("C:/Users/rferreira/My Drive/Altersis/Workspace/ci-testing/public/plot.html")

    # Pipeline output location
    output_file("./public/plot.html")

    show(u)

if __name__ == "__main__":
   main(sys.argv[1:])